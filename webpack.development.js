//开发环境webpack.dev.js
const path = require('path');
const merge = require('webpack-merge');
const common = require('./webpack.common.js');
const HtmlwebpackPlugin = require('html-webpack-plugin');

module.exports = merge(common, {
    mode: 'development',
    module: {},
    devtool: 'source-map',
    devServer: {
        open: true,
        hot: true,
        historyApiFallback: true, // 该配置默认为false，这样react-router只有从首页进入的时候有用，直接访问带路由的地址都是无效的，必须手动开启
        // proxy: {
        //     '/api/': {
        //         target: 'http://baidu.com',
        //         secure: false,
        //         changeOrigin: true,
        //     },
        // },
    },
    plugins: [
        new HtmlwebpackPlugin({
            title: 'website development',
            favicon: 'public/favicon.ico',
            template: 'public/index.html',
        }),
    ],
});
