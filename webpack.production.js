//开发环境webpack.dev.js
const path = require('path');
const merge = require('webpack-merge');
const common = require('./webpack.common.js');
const mode = process.env.mode;

// 生成代码分析报告，帮助提升代码质量和网站性能。
const analyzer = process.argv.indexOf('--analyzer') >= 0;
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
// 生成html并且自动插入css和js的插件
const HtmlwebpackPlugin = require('html-webpack-plugin');
// 生成md5
const WebpackMd5Hash = require('webpack-md5-hash');

// 所有插件
const plugins = [
    new WebpackMd5Hash(),
    // 所有htmlwebpackplugin参考https://github.com/jantimon/html-webpack-plugin#options
    new HtmlwebpackPlugin({
        title: 'website production',
        favicon: 'public/favicon.ico',
        minify: true,
        template: 'public/index.html',
    }),
];

// 只有分析模式才启用分析插件
if (analyzer) {
    plugins.push(new BundleAnalyzerPlugin());
}

module.exports = merge(common, {
    mode: mode,
    module: {},
    devtool: mode === 'development' ? 'eval-source-map' : 'source-map',
    plugins,
    optimization: {
        // 分包异步加载，参考https://imweb.io/topic/5b66dd601402769b60847149
        splitChunks: {
            chunks: 'async',
            minSize: 30000,
            maxSize: 0,
            minChunks: 1,
            maxAsyncRequests: 5,
            maxInitialRequests: 3,
            automaticNameDelimiter: '~',
            name: true,
            cacheGroups: {
                vendors: {
                    test: /[\\/]node_modules[\\/]/,
                    priority: -10,
                },
                default: {
                    minChunks: 2,
                    priority: -20,
                    reuseExistingChunk: true,
                },
            },
        },
    },
    // 需要排除的js
    externals: [],
});
