const express = require('express');
const path = require('path');
const fs = require('fs');

const app = express();

// 设置build目录为公共静态资源目录，禁用index.html，否则所有请求会自动访问build/index.html
app.use(express.static(path.join(__dirname, 'build'), { index: false }));

// 同步读取文件
const readJsonFileSync = (filepath, encoding = 'utf8') => {
    const file = fs.readFileSync(filepath, encoding);
    return JSON.parse(file);
};

// 渲染html
const getRenderHtml = ({ title = '', styles = '', scripts = '' }) => `
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <link rel="icon" href="favicon.ico" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="description" content="" />
        <title>${title}</title>
        ${styles}
    </head>
    <body>
        <noscript>You need to enable JavaScript to run this app.</noscript>
        <div id="root"></div>
        ${scripts}
    </body>
</html>
`;

// 必须使用'*'，这样才会使用react-router的路由，如果使用'/'，那么会启用express的路由
app.get('*', function(req, res) {
    // 下面注释的代码可以直接用来渲染静态html，但是无法插入对象数据，并且所有资源都是相对路径
    // res.sendFile(path.join(__dirname + '/index.html'));

    // 手动渲染html，插入所需要的各种数据和环境变量
    // 检索js和css
    const manifest = readJsonFileSync(__dirname + '/build/manifest.json');
    let styles = [];
    let scripts = [];
    for (const resName in manifest) {
        if (/\.css$/i.test(resName)) {
            styles.push(`<link href="${manifest[resName]}" rel="stylesheet" />`);
        } else if (/\.js$/i.test(resName)) {
            scripts.push(`<script type="text/javascript" src="${manifest[resName]}"></script>`);
        }
    }

    res.send(
        getRenderHtml({
            title: 'website',
            styles: styles.join(''),
            scripts: scripts.join(''),
        }),
    );
});

app.listen(3000, function() {
    console.log('Example app listening on port 3000!');
});
