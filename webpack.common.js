const path = require('path');
const webpack = require('webpack');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const ManifestPlugin = require('webpack-manifest-plugin');

// 分离css到独立文件的loader，css和less共享
const minicssloader = {
    loader: MiniCssExtractPlugin.loader,
    options: {
        // 这里可以指定一个 publicPath
        // 默认使用 webpackOptions.output中的publicPath
        // publicPath的配置，和plugins中设置的filename和chunkFilename的名字有关
        // 如果打包后，background属性中的图片显示不出来，请检查publicPath的配置是否有误
        publicPath: '../',
        // only enable hot in development
        hmr: process.env.NODE_ENV === 'development',
        // if hmr does not work, this is a forceful method.
        reloadAll: true,
    },
};

module.exports = {
    mode: '', // 留空，在dev里面设为development，在prduction里面设为production
    resolve: {
        modules: ['node_modules'],
        extensions: ['.ts', '.tsx', '.js', '.jsx', '.json'],
    },
    entry: ['./src/index.tsx'],
    output: {
        // 所有hash内容参考https://juejin.im/post/5a4502be6fb9a0450d1162ed
        filename: 'scripts/bundle.[name].[hash].js',
        path: path.resolve(__dirname, 'build'),
        chunkFilename: 'scripts/[hash].[id].chunk.js',
        publicPath: '', // 此处可以设置默认cdn地址
    },
    module: {
        rules: [
            {
                test: /\.ts(x?)$/,
                exclude: /node_modules/,
                include: [path.resolve(__dirname, 'src')],
                loader: 'ts-loader',
            },
            {
                test: /\.js(x?)$/,
                include: [path.resolve(__dirname, 'src')],
                exclude: /node_modules/,
                // exclude: /node_modules\/(?!react-intl|intl-messageformat|intl-messageformat-parser)/, // 此处配置没有生效，可能和windows的文件路径\有关，以后研究
                loader: 'babel-loader',
            },
            {
                test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[name].[ext]',
                            outputPath: 'fonts/',
                        },
                    },
                ],
            },
            // 专门处理类似import img from './image.png';这样的代码
            // 参考https://www.npmjs.com/package/url-loader
            // {
            //     test: /\.(png|jpg|gif)$/i,
            //     use: [
            //         {
            //             loader: 'url-loader',
            //             options: {
            //                 limit: 8192,
            //             },
            //         },
            //     ],
            // },
            // 处理less 参考https://www.npmjs.com/package/less-loader
            {
                test: /\.(png|svg|jpg|gif)$/i,
                loader: 'file-loader',
                options: {
                    name: 'assets/images/[name].[ext]',
                },
            },
            {
                test: /\.less$/i,
                use: [
                    minicssloader,
                    {
                        loader: 'css-loader',
                        options: {
                            sourceMap: process.env.NODE_ENV === 'development',
                            modules: true, // 开启css module，默认为false，会导致less无法启用模块化
                        },
                    },
                    {
                        loader: 'less-loader',
                    },
                ],
            },
            // 分离css到单独文件
            {
                test: /\.css$/i,
                use: [
                    minicssloader,
                    {
                        loader: 'css-loader',
                    },
                    {
                        loader: 'less-loader',
                    },
                ],
            },
        ],
    },
    plugins: [
        new MiniCssExtractPlugin({
            // Options similar to the same options in webpackOptions.output
            // all options are optional
            filename: 'styles/[name].[hash].css',
            chunkFilename: 'styles/[id].[hash].css',
            ignoreOrder: false, // Enable to remove warnings about conflicting order
        }),
        new ManifestPlugin(), //生产资源目录，node express会读取该文件， 参考https://www.npmjs.com/package/webpack-manifest-plugin
        new webpack.DefinePlugin({
            // 定义全局变量，参考https://webpack.js.org/plugins/define-plugin/
            // 除了Number和Boolean，其他数据类型都要加上JSON.stringify()
            __isDEV__: process.env.NODE_ENV === 'development',
        }),
    ],
};
