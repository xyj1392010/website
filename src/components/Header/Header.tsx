import * as React from 'react';

import './style.css';

/**
 * Header 全局公用头部
 */
export default class Header extends React.Component {
    render() {
        return (
            <header className="header">
                <h1>Header</h1>
            </header>
        );
    }
}
