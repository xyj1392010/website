import * as React from 'react';

import style from './style.less';

/**
 * Header 全局公用头部
 */
export default class Footer extends React.Component {
    render() {
        return (
            <footer className={style.footer}>
                <h1>Footer</h1>
            </footer>
        );
    }
}
