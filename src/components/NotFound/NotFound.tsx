import * as React from 'react';
import { Redirect } from 'react-router-dom';

export default class NotFound extends React.Component {
    componentDidMount() {}
    render() {
        return (
            <>
                <h1>404</h1>
                {/* 使用Redirect来做页面跳转 */}
                <Redirect to={{ pathname: '/home' }} />
            </>
        );
    }
}
