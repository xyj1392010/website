import * as React from 'react';

interface HelloProps {
    props1: string;
    props2: string;
}

interface HelloState {
    state1: string;
    state2: string;
}

// test
export default class HelloWorld extends React.Component<HelloProps, HelloState> {
    render() {
        return (
            <h1>
                Hello from {this.props.props1} and {this.props.props2}!
            </h1>
        );
    }
}
