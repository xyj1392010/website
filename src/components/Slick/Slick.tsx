import * as React from 'react';
import Slider from 'react-slick';

import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

import './style.css';

/**
 * 轮播
 */
export default class Slick extends React.Component {
    render() {
        const settings = {
            className: 'center',
            centerMode: true,
            infinite: true,
            dots: true,
            //centerPadding: '60px',
            slidesToShow: 1,
            speed: 500,
            variableWidth: true,
        };
        return (
            <div className="">
                <Slider {...settings}>
                    <div className="item" style={{ width: 800 }}>
                        <h3>1</h3>
                    </div>
                    <div className="item" style={{ width: 800 }}>
                        <h3>2</h3>
                    </div>
                    <div className="item" style={{ width: 800 }}>
                        <h3>3</h3>
                    </div>
                </Slider>
            </div>
        );
    }
}
