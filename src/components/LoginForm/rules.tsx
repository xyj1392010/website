import React from 'react';
import { FormattedMessage } from 'react-intl/dist';
import { FormInstance } from 'antd/lib/form';
import i18n from '../../i18n/index';

// w3c使用的email正则：https://html.spec.whatwg.org/multipage/input.html#valid-e-mail-address
const regEmail = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

/**
 * 用户名的验证规则
 * @param {string} lang 语言
 */
export const rulesUserName = (lang: string) => [
    (form: FormInstance) => ({
        validator: (rule: any, value: any) => {
            if (!value || !regEmail.test(value)) {
                return Promise.reject(i18n[lang].login.username_required);
            }

            return Promise.resolve();
        },
    }),
];

/**
 * 密码的验证规则
 * @param {string} lang 语言
 */
export const rulesPassword = (lang: string) => [
    (form: FormInstance) => ({
        validator: (rule: any, value: any) => {
            if (!value || value.length <= 7) {
                return Promise.reject(i18n[lang].login.password_required);
            }

            return Promise.resolve();
        },
    }),
];
