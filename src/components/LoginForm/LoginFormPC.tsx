import * as React from 'react';
import { connect } from 'react-redux';
import { Form, Input, Button, Checkbox } from 'antd';
import { IntlProvider, FormattedMessage } from 'react-intl/dist';
import i18n from '../../i18n/index';
import './stylePC.css';
import { rulesUserName, rulesPassword } from './rules';

export interface LoginFormProps {
    lang?: string;
}

@(connect((state: any) => ({
    lang: state.app.lang,
})) as any)
/**
 * 登录/注册表单
 */
export default class LoginForm extends React.Component<LoginFormProps> {
    // 验证全部通过后触发，formValues是一个form表单内容的键值对
    onFinish = (formValues: any) => {
        console.log('LoginForm onFinish');
        console.log(formValues);
    };

    onFinishFailed = ({ values, errorFields, outOfDate }: { values: any; errorFields: any; outOfDate: any }) => {
        console.log('LoginForm onFinishFailed');
    };

    render() {
        const { lang } = this.props;

        return (
            <IntlProvider locale={lang} messages={i18n[lang].login}>
                <h3 className="title" style={{ display: 'block' }}>
                    <FormattedMessage id="title_login" />
                </h3>
                <h3 className="title" style={{ display: 'none' }}>
                    <FormattedMessage id="title_register" />
                </h3>
                <Form name="loginForm" labelAlign="right" onFinish={this.onFinish} onFinishFailed={this.onFinishFailed}>
                    <Form.Item label="Username" hasFeedback name="username" rules={rulesUserName(lang)}>
                        <Input placeholder={i18n[lang].login.username_placeholder} />
                    </Form.Item>
                    <Form.Item label="Password" name="password" rules={rulesPassword(lang)}>
                        <Input.Password placeholder={i18n[lang].login.password_placeholder} />
                    </Form.Item>
                    {/*<Form.Item name="remember" valuePropName="checked"><Checkbox>Remember me</Checkbox></Form.Item>*/}
                    <Form.Item>
                        <Button type="primary" htmlType="submit">
                            <FormattedMessage id="login_submit" />
                        </Button>
                    </Form.Item>
                </Form>
            </IntlProvider>
        );
    }
}
