import * as React from 'react';
import { connect } from 'react-redux';
import { Modal } from 'antd';
import { actions } from '../redux/modules/modal';

import LoginFormPC from '../components/LoginForm/LoginFormPC';

export interface ModalManagerProps {
    modalList?: string[];
    hideModalPCAction?: (modalName: string) => {};
}

@(connect(
    (state: any) => ({
        modalList: state.modal.modalList,
    }),
    actions,
) as any)
/**
 * modal管理器
 */
export default class ModalManager extends React.Component<ModalManagerProps> {
    constructor(props: any) {
        super(props);

        console.log(props);
    }

    getModalComponent(modalName: string) {
        let component = null;

        switch (modalName) {
            case 'LoginFormPC':
                component = <LoginFormPC />;
            default:
                break;
        }

        return component;
    }

    render() {
        const { modalList } = this.props;

        return (
            <>
                {modalList.map((item: any, index: number) => {
                    const modalName = item.modalName;
                    const loginModalOptions = {
                        visible: true,
                        footer: '',
                        centered: true,
                        onCancel: () => {
                            this.props.hideModalPCAction(modalName);
                        },
                        wrapClassName: modalName,
                    };

                    return (
                        <Modal key={modalName} {...loginModalOptions}>
                            {this.getModalComponent(modalName)}
                        </Modal>
                    );
                })}
            </>
        );
    }
}
