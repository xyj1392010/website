// action
const actionTypes = {
    INCREMENT: 'INCREMENT',
};

//  init state
const initialState = {
    count: 0,
};

// action creator
export const actions = {
    increaseAction: () => {
        // 同步action
        // return { type: actionTypes.INCREMENT };
        // 异步action，使用了redux-thunk中间件
        return (dispatch: any, getState: any) => {
            new Promise((reslove, reject) => {
                setTimeout(() => {
                    reslove();
                }, 5000);
            }).then(() => {
                dispatch({ type: actionTypes.INCREMENT });
            });
        };
    },
};

// reducer
export default function counter(state = initialState, actions: any = {}) {
    switch (actions.type) {
        case actionTypes.INCREMENT:
            return { count: state.count + 1 };
        default:
            return state;
    }
}
