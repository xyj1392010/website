// action
const actionTypes = {
    SHOWLOGINPC: 'SHOWLOGINPC',
    HIDELOGINPC: 'HIDELOGINPC',
    HIDEALLPC: 'HIDEALLPC',
};

//  init state
const initialState = {
    modalList: new Array<any>(), // 弹窗的name
};

// action creator
export const actions = {
    showModalPCAction: (modalName: string, modalData: any = {}) => {
        return { type: actionTypes.SHOWLOGINPC, modalName, modalData };
    },
    hideModalPCAction: (modalName: string, modalData: any = {}) => {
        return { type: actionTypes.HIDELOGINPC, modalName, modalData };
    },
    hideAllPCAction: () => {
        return { type: actionTypes.HIDEALLPC };
    },
};

// reducer
export default function app(state = initialState, actions: any = {}) {
    switch (actions.type) {
        case actionTypes.SHOWLOGINPC:
            return { modalList: [...state.modalList, { modalName: actions.modalName, modalData: actions.modalData }] };
        case actionTypes.HIDELOGINPC:
            return { modalList: [...state.modalList].filter(value => value.modalName !== actions.modalName) };
        case actionTypes.HIDEALLPC:
            return { modalList: new Array<string>() };
        default:
            return state;
    }
}
