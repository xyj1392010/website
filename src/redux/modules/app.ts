// action
const actionTypes = {
    INITLANG: 'INITLANG',
};

//  init state
const initialState = {
    lang: 'zh-Hant', // 默认繁体中文
};

// action creator
export const actions = {
    initLangAction: () => {
        return { type: actionTypes.INITLANG };
    },
};

// reducer
export default function app(state = initialState, actions: any = {}) {
    switch (actions.type) {
        case actionTypes.INITLANG:
            return { lang: state.lang };
        default:
            return state;
    }
}
