import { combineReducers } from 'redux';

import counter from './modules/counter';
import app from './modules/app';
import modal from './modules/modal';

// 引入所有modules下的reducer
export default combineReducers({
    app,
    modal,
    counter,
});
