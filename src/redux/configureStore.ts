import { createStore, applyMiddleware, compose } from 'redux';

import loggerMiddleware from './middleware/logger';
import thunkMiddleware from 'redux-thunk';

import rootReducer from './reducer';

export default function configureStore(preloadedState: any = {}) {
    // 中间件的顺序不能错：日志记录-处理异步-等等...
    const middlewares = [loggerMiddleware, thunkMiddleware];
    const middlewareEnhancer = applyMiddleware(...middlewares);

    // 增加chrome上redux调式插件，此方式为非侵入式，参考http://extension.remotedev.io/#usage
    const composedEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

    // 第一个参数是标准reducer
    // 第二个参数可选，初始化state
    // 第三个参数可选，增强store
    const store = createStore(rootReducer, preloadedState, composedEnhancers(middlewareEnhancer));

    return store;
}
