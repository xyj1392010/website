export default {
    username_required: '不是有效的邮箱或ID',
    password_required: '密码至少8位字符',
    username_placeholder: '请输入游戏或ID',
    password_placeholder: '请输入密码',
    login_submit: '登录',
    title_login: '欢迎登录',
    title_register: '账号注册',
};
