import * as en from './en/index';
import * as ja from './ja/index';
import * as ko from './ko/index';
import * as zhHant from './zh-Hant/index';

export default {
    en,
    ja,
    ko,
    'zh-Hant': zhHant,
} as any;
