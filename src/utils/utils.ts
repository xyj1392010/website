import axios, { AxiosRequestConfig } from 'axios';
import Cookies from 'js-cookie';

// cookie
window._cookie = Cookies;

/**
 * http请求
 * @param {string} url 请求地址
 * @param {AxiosRequestConfig} options 请求的参数配置
 */
export function fetchData(url: string, options: AxiosRequestConfig = {}) {
    return axios(url, options);
}

/**
 * 获取url上参数
 * @param {string} name 可选，参数的name，如果不传，返回所有queryParams
 */
export function getQuertParams(name?: string) {
    const query = window.location.search.substring(1);
    const vars = query.split('&');
    const query_string: any = {};
    for (let i = 0; i < vars.length; i++) {
        let pair = vars[i].split('=');
        let key = decodeURIComponent(pair[0]);
        let value = decodeURIComponent(pair[1]);
        // If first entry with this name
        if (typeof query_string[key] === 'undefined') {
            query_string[key] = decodeURIComponent(value);
            // If second entry with this name
        } else if (typeof query_string[key] === 'string') {
            let arr = [query_string[key], decodeURIComponent(value)];
            query_string[key] = arr;
            // If third or later entry with this name
        } else {
            query_string[key].push(decodeURIComponent(value));
        }
    }
    return name ? query_string[name] : query_string;
}

/**
 * 获取当前语言
 */
export function getLang() {
    // 标准语言列表参考：http://www.lingoes.net/en/translator/langcode.htm
    // 语言规范的支持相对滞后，额外可以参考：https://www.zhihu.com/question/20797118

    // 系统语言
    let lang = window.navigator.language;
    // cookie存储的语言
    let cookieLang = Cookies.get('lang');
    // url参数上传递的语言
    let queryLang = getQuertParams('lang');

    // 如果cookie有存，那么使用cookie里面的
    if (cookieLang) {
        lang = cookieLang;
    }

    // 如果有传递lang，最高优先级
    if (queryLang) {
        lang = queryLang;

        // 重新设置cookie，默认100年
        let CookieDate = new Date();
        CookieDate.setFullYear(CookieDate.getFullYear() + 10);
        Cookies.set('lang', lang, { expires: CookieDate });
    }

    // 所有中文默认按照繁体中文展示
    if (lang.indexOf('zh') === 0) {
        lang = 'zh-Hant';
    } else if (lang === 'ko' || lang === 'ko-KR') {
        lang = 'ko';
    } else if (lang === 'jp') {
        lang = 'jp';
    } else {
        lang = 'en';
    }

    // 暂时写死
    lang = 'zh-Hant';

    return lang;
}
