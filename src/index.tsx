import '@babel/polyfill'; // 兼容低版本ie
import * as React from 'react';
import * as ReactDOM from 'react-dom';

import { Provider } from 'react-redux';
import configureStore from './redux/configureStore';
import App from './app';

import { getLang } from './utils/utils';

// 初始化state
const preloadedState = {
    app: {
        lang: getLang(),
    },
    counter: {
        count: 100,
    },
};

// 初始化store
const store = configureStore(preloadedState);
// 只在开发模式下监听store的变化并且输出为全局变量
if (__isDEV__) {
    window._store = store.getState();
    store.subscribe(() => {
        window._store = store.getState();
    });
}

ReactDOM.render(<Provider store={store}>{<App />}</Provider>, document.getElementById('root'));
