import * as React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import Counter from '../Counter/Counter';
import { IntlProvider, FormattedMessage } from 'react-intl/dist';

import './style.less';
import i18n from '../../i18n/index';

export interface HomeProps {
    lang?: string;
}

// 这里演示了只要props的情况
@(connect((state: any) => ({
    lang: state.app.lang,
})) as any)
export default class Home extends React.Component<HomeProps> {
    constructor(props: any) {
        super(props);

        console.log(i18n);
    }

    render() {
        const { lang } = this.props;
        console.log(lang);

        return (
            <IntlProvider locale={lang} messages={i18n[lang].home}>
                <div className="home">
                    <h1>
                        <FormattedMessage id="index" />
                    </h1>
                    <Link to="/about">About</Link>
                    <Link to="/login">Login</Link>
                    withRouter
                    <Counter />
                </div>
            </IntlProvider>
        );
    }
}
