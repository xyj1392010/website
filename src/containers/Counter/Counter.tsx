import * as React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { actions } from '../../redux/modules/counter';

export interface CounterProps {
    value?: number;
    increaseAction?: () => void;
    history?: any;
    lang?: string;
}

@(withRouter as any)
@(connect(
    (state: any) => ({
        value: state.counter.count,
        lang: state.app.lang,
    }),
    actions,
) as any)
export default class Counter extends React.Component<CounterProps> {
    constructor(props: any) {
        super(props);

        this.routergo = this.routergo.bind(this);
        this.onClickIncrease = this.onClickIncrease.bind(this);
    }

    onClickIncrease() {
        this.props.increaseAction();
    }

    routergo() {
        // this.props.history.goBack();
        // this.props.history.go(-1);
        this.props.history.push('/login');
    }

    render() {
        const { value } = this.props;

        return (
            <div>
                <span>{value}</span>
                <button onClick={this.onClickIncrease}>Increase</button>
                <button onClick={this.routergo}>页面跳转测试</button>
            </div>
        );
    }
}

// function mapStateToProps(state: any) {
//     return {
//         value: state.counter.count,
//     };
// }

// function mapDispatchToProps(dispatch: any) {
//     return {
//         increaseAction: () => dispatch(actions.increaseAction()),
//     };
// }

// export default connect(mapStateToProps, mapDispatchToProps)(Counter);
