import * as React from 'react';
import { connect } from 'react-redux';
import { Modal, Button } from 'antd';
import { actions } from '../../redux/modules/modal';

import './style.less';

export interface LoginProps {
    showModalPCAction?: (modalName: string, modalData?: any) => {};
    hideModalPCAction?: (modalName: string) => {};
}

// 只要action，不要state
@(connect(null, actions) as any)
export default class Login extends React.Component<LoginProps> {
    constructor(props: any) {
        super(props);
    }

    handleOpenModal = () => {
        this.props.showModalPCAction('LoginFormPC', { testdata: 1 });
    };

    handleModalCancel = () => {
        this.props.hideModalPCAction('LoginFormPC');
    };

    render() {
        return (
            <div className="login">
                <h1>Login</h1>
                <Button type="primary" onClick={this.handleOpenModal}>
                    打开登录框
                </Button>
                {/*<LoginModalPC loginVisible={loginVisible} />*/}
            </div>
        );
    }
}
