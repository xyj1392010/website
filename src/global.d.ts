// 声明less文件
declare module '*.less' {
    const classes: { [key: string]: string };
    export default classes;
}

// 声明json文件
declare module '*.json' {
    const value: any;
    export default value;
}

// window下挂载对象申明
declare interface Window {
    __REDUX_DEVTOOLS_EXTENSION_COMPOSE__?: any;
    _cookie: any;
    _store: any;
}

// 全局变量声明
// 所有在webpack的DefinePlugin中定义的全局变量都要在此声明为typescript能识别的全局变量
declare const __isDEV__: boolean; // 是否是开发环境

declare module 'intl' {
    const file: any;
    export = file;
}

declare module 'intl/locale-data/jsonp/en.js' {
    const file: any;
    export = file;
}

declare module 'intl/locale-data/jsonp/zh.js' {
    const file: any;
    export = file;
}

declare module 'intl/locale-data/jsonp/ja.js' {
    const file: any;
    export = file;
}

declare module 'intl/locale-data/jsonp/ko.js' {
    const file: any;
    export = file;
}
