import * as React from 'react';
/* 原始URL地址是这样子的：http://localhost:3000/#/?xxxxx
   如果想要移除#，那么要使用BrowserRouter
   另外一个是HashRouter，参考https://blog.csdn.net/gwdgwd123/article/details/85029121
*/
import { BrowserRouter, Switch, Route, Link } from 'react-router-dom';

import { About, Home, Login } from './containers/index';
import Header from './components/Header/Header';
import Footer from './components/Footer/Footer';
import NotFound from './components/NotFound/NotFound';
import Slick from './components/Slick/Slick';
import ModalManager from './modal/ModalManager';

import 'antd/dist/antd.css';

export default function Routes() {
    return (
        <BrowserRouter>
            <Header />
            <Slick />
            {/* <Switch>会迭代它下面的所有<Route>子组件，并只渲染第一个路径匹配的<Route> */}
            <Switch>
                <Route path="/home">
                    <Home />
                </Route>
                <Route path="/about">
                    <About />
                </Route>
                <Route path="/login">
                    <Login />
                </Route>
                {/* 如果上面的Route的路径都没有匹配上，我们可以开发一个<NotFound>组件，在其中返回404 */}
                <Route>
                    <NotFound />
                </Route>
            </Switch>
            {/* 弹窗管理器 */}
            <ModalManager />
            <Footer />
        </BrowserRouter>
    );
}
