import * as React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { actions } from './redux/modules/app';
import Routes from './routes';

export interface AppProps {
    lang?: string;
}

@(connect(
    (state: any) => ({
        lang: state.app.lang,
    }),
    actions,
) as any)
export default class App extends React.Component<AppProps, {}> {
    constructor(props: any) {
        super(props);
    }
    render() {
        return (
            <>
                <Routes />
            </>
        );
    }
}
